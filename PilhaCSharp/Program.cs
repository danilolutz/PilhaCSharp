﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PilhaCSharp
{
    class Program
    {
        static void Main()
        {
            //PILHA - Usando a classe Stack que implementa collections.
            Stack<int> objPilha = new Stack<int>();

            // PUSH
            int length = 10;
            for (int i = 1; i <= length; i++)
            {
                objPilha.Push(i);
            }

            foreach (int i in objPilha)
            {
                Console.WriteLine(i);
            }

            // MIN
            Console.WriteLine(string.Format("\nO menor valor é: {0}\n", objPilha.Min()));

            // MAIOR VALOR ANTES DO POP
            Console.WriteLine(string.Format("\nO maior valor é: {0}\n", objPilha.Max()));

            // POP
            objPilha.Pop();

            // MAIOR VALOR DEPOIS DO POP
            Console.WriteLine(string.Format("\nO maior valor depois do pop é: {0}\n", objPilha.Max()));
            foreach (int i in objPilha)
            {
                Console.WriteLine(i);
            }

            Console.Read();
        }
    }
}
